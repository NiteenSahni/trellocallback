/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

let fs = require('fs')
let path = require('path')

function problem2(boardID, callback2) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, 'lists.json'), (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let stoneObj = JSON.parse(data)
                let id = boardID
                let resultArr = callback2(err, stoneObj[id])

                return resultArr
            }
        })
    }, 2 * 1000);
}
module.exports = problem2
