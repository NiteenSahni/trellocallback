
/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

let fs = require("fs")
let path = require("path");



function problem1(boardID, callback1) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, "boards.json"), (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let dataArray = JSON.parse(data)
                let villianData = callback1(err, dataArray.filter((villianDetails) => {
                    return villianDetails.id === boardID;
                }))

                return villianData;

            }
        })
    }, 2 * 100);
}




module.exports = problem1;