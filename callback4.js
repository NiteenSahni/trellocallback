/* 
    Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

let fs = require('fs')
let path = require('path')
let dataOfOne = require('./callback1')
let dataOfTwo = require('./callback2')
let dataOfThree = require('./callback3')
function problem4(name) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, 'boards.json'), (err, data) => {
            if (err) {
                console.log(err);
            }
            else {
                let charInfo = JSON.parse(data).filter((info) => {
                    return info.name == name
                })

                let id = charInfo[0].id

                dataOfOne(id, (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {

                        console.log(data)

                        dataOfTwo(id, (err, data) => {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log(data);
                                let mindData = data.filter((info) => {
                                    return info.name == 'Mind'
                                })

                                let mindDataId = mindData[0].id

                                dataOfThree(mindDataId, (err, data) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log(data);
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }, 2 * 1000)
}


module.exports = problem4;
