/* 
    Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

let fs = require('fs')
let path = require('path')
let dataOfOne = require('./callback1')
let dataOfTwo = require('./callback2')
let dataOfThree = require('./callback3')
function problem5(name) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, 'boards.json'), (err, data) => {
            if (err) {
                console.log(err);
            }
            else {
                let charInfo = JSON.parse(data).filter((info) => {
                    return info.name == name
                })

                let id = charInfo[0].id

                dataOfOne(id, (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {

                        console.log(data)

                        dataOfTwo(id, (err, data) => {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log(data);

                                let mindData = []
                                mindData = (data.filter((info) => {
                                    if (info.name == 'Mind' || info.name == 'Space') {
                                        return info

                                    }
                                })).map((idInfo) => {
                                    return idInfo.id
                                })
                                for (let index = 0; index < mindData.length; index++) {

                                    dataOfThree(mindData[index], (err, data) => {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log(data);
                                        }
                                    })
                                }
                            }
                        })
                    }
                })
            }
        })
    }, 2 * 1000)
}
module.exports = problem5

