/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

let fs = require('fs')
let path = require('path')

function problem3(listID, callback3) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, 'cards.json'), (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let cardObj = JSON.parse(data)

                let id = listID
                let resultArr = callback3(err, cardObj[id])
                return resultArr;
            }
        })
    }, 2 * 1000);
}
module.exports = problem3
